<?php

if (isset($_POST['Submit'])) {

    if ($_POST['inputFirstName'] != "") {
        $_POST['inputFirstName'] = filter_var($_POST['inputFirstName'], FILTER_SANITIZE_STRING);
        if ($_POST['inputFirstName'] == "") {
            $errors .= 'Vul aub een geldige voornaam in.<br/><br/>';
        }
    } else {
        $errors .= 'Vul aub uw voornaam in.<br/>';
    }

    if ($_POST['inputName'] != "") {
        $_POST['inputName'] = filter_var($_POST['inputName'], FILTER_SANITIZE_STRING);
        if ($_POST['inputName'] == "") {
            $errors .= 'Vul aub een geldige naam in.<br/><br/>';
        }
    } else {
        $errors .= 'Vul aub uw naam in.<br/>';
    }

    if ($_POST['inputAddress'] != "") {
        $_POST['inputAddress'] = filter_var($_POST['inputAddress'], FILTER_SANITIZE_STRING);
        if ($_POST['inputAddress'] == "") {
            $errors .= 'Vul aub een geldige adres in.<br/><br/>';
        }
    } else {
        $errors .= 'Vul aub uw adres in.<br/>';
    }

    if ($_POST['inputEmail'] != "") {
        $email = filter_var($_POST['inputEmail'], FILTER_SANITIZE_EMAIL);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors .= "$email is <strong>GEEN</strong> geldig email adres.<br/><br/>";
        }
    } else {
        $errors .= 'Vul aub uw email in.<br/>';
    }


    if (!$errors) {
        $mail_to = 'lucia.morgan@gmail.com';
        $subject = 'New Mail from Form Submission';
        $message = 'From: ' . $_POST['inputName'] . "\n";
        $message .= 'Email: ' . $_POST['email'] . "\n";
        mail($to, $subject, $message);

        echo "Thank you for your email!<br/><br/>";
    } else {
        echo '<div style="color: red">' . $errors . '<br/></div>';
    }
}
?>
