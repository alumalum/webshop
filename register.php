<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Webshop | Registratie</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <style> 
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar 
                }

            </style>
            <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

            <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
            <!--[if lt IE 9]>
              <script src="assets/js/html5shiv.js"></script>
            <![endif]-->

            <!-- Fav and touch icons -->
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
            <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
            <link rel="shortcut icon" href="assets/ico/favicon.png">
        </head>

        <body>

            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="brand" href="#">Webshop</a>
                        <div class="nav-collapse collapse">
                            <ul class="nav">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="#about">Wie zijn wij</a></li>
                                <li class="active"><a href="register.php">Registratie</a></li>
                                <li><a href="#contact">Contact</a></li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>

            <div class="container">

                <h1>Registratie</h1>
                <p>Vul aub uw gegevens in.</p>
                <p class="text-warning">Opgelet: Alle velden zijn verplicht.</p>

                <form name="register" class="form-horizontal" method="post" action="register.php">
                    <div class="control-group">
                        <label class="control-label" for="inputFirstName">Voornaam</label>
                        <div class="controls">
                            <input type="text" id="inputFirstName" 
                                   value="<?php echo $_POST['inputFirstName']; ?>" placeholder="Voornaam">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputName">Naam</label>
                        <div class="controls">
                            <input type="text" id="inputName" 
                                   value="<?php echo $_POST['inputName']; ?>" placeholder="Naam">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputAddress">Adres</label>
                        <div class="controls">
                            <input type="text" id="inputAddress" 
                                   value="<?php echo $_POST['inputAddress']; ?>" placeholder="Adres">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputPhone">Telefoon</label>
                        <div class="controls">
                            <input type="text" id="inputPhone" 
                                   value="<?php echo $_POST['inputPhone']; ?>" placeholder="Telefoon">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Email</label>
                        <div class="controls">
                            <input type="text" id="inputEmail" 
                                   value="<?php echo $_POST['inputEmail']; ?>" placeholder="Email">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputPassword">Wachtwoord</label>
                        <div class="controls">
                            <input type="password" id="inputPassword" 
                                   value="<?php echo $_POST['inputPassword']; ?>" placeholder="Wachtwoord">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <label class="checkbox">
                                <input type="checkbox"> Ja, ik wil de <a href="#">Webshop Nieuwsbrief</a> via email krijgen.
                            </label>
                            <label class="checkbox">
                                <input type="checkbox"> Ik accepteer de <a href="#">algemene voorwaarden</a>.
                            </label>
                            <button type="submit" class="btn">Registreer mij</button>
                        </div>
                    </div>
                </form>

            </div> <!-- /container -->

            <!-- Le javascript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="assets/js/jquery.js"></script>
            <script src="assets/js/bootstrap-transition.js"></script>
            <script src="assets/js/bootstrap-alert.js"></script>
            <script src="assets/js/bootstrap-modal.js"></script>
            <script src="assets/js/bootstrap-dropdown.js"></script>
            <script src="assets/js/bootstrap-scrollspy.js"></script>
            <script src="assets/js/bootstrap-tab.js"></script>
            <script src="assets/js/bootstrap-tooltip.js"></script>
            <script src="assets/js/bootstrap-popover.js"></script>
            <script src="assets/js/bootstrap-button.js"></script>
            <script src="assets/js/bootstrap-collapse.js"></script>
            <script src="assets/js/bootstrap-carousel.js"></script>
            <script src="assets/js/bootstrap-typeahead.js"></script>

        </body>
    </html>
